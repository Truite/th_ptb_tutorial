%% clear
clear all global
restoredefaultpath

%% add the path to th_ptb
addpath('/home/th/git/th_ptb/') % change this to where th_ptb is on your system

%% initialize the PTB
th_ptb.init_ptb('/home/th/git_other/Psychtoolbox-3/'); % change this to where PTB is on your system

%% get a configuration object
ptb_cfg = th_ptb.PTB_Config();

%% configure button mappings
ptb_cfg.datapixxresponse_config.button_mapping('target') = ptb_cfg.datapixxresponse_config.Green;
ptb_cfg.keyboardresponse_config.button_mapping('target') = KbName('space');

ptb_cfg.datapixxresponse_config.button_mapping('other_target') = ptb_cfg.datapixxresponse_config.Red;
ptb_cfg.keyboardresponse_config.button_mapping('other_target') = KbName('RightShift');

%% get th_ptb.PTB object
ptb = th_ptb.PTB.get_instance(ptb_cfg);

%% init audio and triggers
ptb.setup_response;

%% wait one second for target key
ptb.wait_for_keys('target', GetSecs+1)

%% wait 10 seconds for either key
ptb.wait_for_keys({'target', 'other_target'}, GetSecs+10)