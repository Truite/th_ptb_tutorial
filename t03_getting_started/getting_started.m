%% clear
clear all global
restoredefaultpath

%% add the path to th_ptb
addpath('/home/th/git/th_ptb/') % change this to where th_ptb is on your system

%% initialize the PTB
th_ptb.init_ptb('/home/th/git_other/Psychtoolbox-3/'); % change this to where PTB is on your system

%% get a configuration object
ptb_cfg = th_ptb.PTB_Config();

%% do the configuration
ptb_cfg.fullscreen = false;
ptb_cfg.window_scale = 0.2;
ptb_cfg.skip_sync_test = true;
ptb_cfg.hide_mouse = false;

%% get th_ptb.PTB object
ptb = th_ptb.PTB.get_instance(ptb_cfg);

%% init screen
ptb.setup_screen;