%% clear and restore path...
clear all global

restoredefaultpath

%% add the toolbox to the path...
addpath('my_toolbox');

%% get a human and let him say hello
my_pirate = example01.Pirate('Adam');
my_pirate.say_name();
my_pirate.hello();